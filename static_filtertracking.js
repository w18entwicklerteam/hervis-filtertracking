var filterPage = function(){


    if((/store\/search/i).test(location.href)){
        return "search";
    }else{
        return "category";
    }
};

$('#facetRefinementsNav').on('click', function (event) {

    var closestLabel = $(event.target).closest("label")[0];
    var closestParent = $(event.target).closest(".facet")[0];


    if(closestParent.classList.value.includes("facetPromotion") && $(closestParent).find(".disabled").length === 0){


        _trackgate.push({
            track           : "eventvar",
            key             : "filtertracking",
            value           : filterPage()
        });

        _trackgate.push({
            track: "event",
            group: "filtertracking",
            name: closestParent.innerText.trim(),
            value: closestParent.innerText.trim()
        });
    }else if(closestParent.classList.value.includes("facetPromotion") === false){

        var labelText = closestLabel.innerText.substring(0, closestLabel.innerText.indexOf('(')).trim();
        var parentCategory = $(closestParent).find(".facetHead")[0].innerText;


        _trackgate.push({
            track           : "eventvar",
            key             : "filtertracking",
            value           : filterPage()
        });

        _trackgate.push({
            track: "event",
            group: "filtertracking",
            name: parentCategory,
            value: labelText
        });

        console.log(labelText);
        console.log(parentCategory);

    }



});


$('#sort-cell').on('change', function (event) {

    var sortText = $(event.target)[0].selectedOptions[0].innerText.trim();

    _trackgate.push({
        track           : "eventvar",
        key             : "filtertracking",
        value           : filterPage()
    });

    _trackgate.push({
        track: "event",
        group: "filtertracking",
        name: "sort",
        value: sortText
    });
});

$('#online-cell').on('click', function (event) {

    if(event.target.classList.value.includes("disabled") !== true ) {

        _trackgate.push({
            track: "eventvar",
            key: "filtertracking",
            value: filterPage()
        });

        _trackgate.push({
            track: "event",
            group: "filtertracking",
            name: $(".facetOnline")[0].innerText,
            value: $(".facetOnline")[0].innerText
        });
    }
});


$('#reservable-cell').on('click', function (event) {
    if(event.target.classList.value.includes("disabled") !== true ) {

        _trackgate.push({
            track: "eventvar",
            key: "filtertracking",
            value: filterPage()
        });

        _trackgate.push({
            track: "event",
            group: "filtertracking",
            name: $(".facetReservable")[0].innerText,
            value: $(".facetReservable")[0].innerText
        });
    }

});


